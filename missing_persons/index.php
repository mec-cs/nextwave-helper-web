<?php
	include('../session.php');
	if (isset($_GET['del'])){
		$delete = $_GET['del'];
		$message = 
		'<div class="alert bg-success alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> The entry - '.$delete.' was deleted.
		</div>';
	}
	$operationStartDate = "(SELECT OperationStartDate FROM archivedata WHERE OperationID = 1)";
	$operationEndDate = "(SELECT OperationEndDate FROM archivedata WHERE OperationID = 1)";

	$missingPersonListQuery = "SELECT PersonID, Time, Name, Sex, Age, Phone, Location, Missing
							   FROM missingpersons
							   WHERE Time >= $operationStartDate AND
							   (Time <= $operationEndDate OR $operationEndDate IS NULL)
							   ORDER BY Missing DESC;";

	$result = mysqli_query($db, $missingPersonListQuery);
	if (!$result) {
		echo "Error Fetching!";
		exit();
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Missing Persons Database</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<!-- MDBootstrap Datatables  -->
		<link href="../bootstrap/css/datatables.min.css" rel="stylesheet">
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col">
					<h3><a href = "../index.php">⤺ Go Back</a></h3>
				</div>
				<div class="col">
					<h3 ALIGN=RIGHT><a href = "../logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>
				</div>
			</div>
			<div class="row" style="margin-bottom: 20px;">
				<div class = "col-lg-2 text-center">
					<a href="add.php" class="btn btn-primary" role="button"><i class="fas fa-plus-square"></i><br>Add New Entry</a>
				</div>
				<div class="col-lg-10 text-right">
					<span>Legend: </span>
					<div class='btn btn-danger'> Missing </div>
					<div class='btn btn-success'> Found </div>
				</div>
			</div>
			<?php
				if (isset($message)){
					echo $message;
				}
			?>
			<table id="dtable" class="mytable table-dark table-striped table" width="100%">
				<thead>
					<tr>
						<td><strong>Date Reported</strong></td>
						<td><strong>Name</strong></td>
						<td><strong>Sex</strong></td>
						<td><strong>Age</strong></td>
						<td><strong>Phone</strong></td>
						<td><strong>Location</strong></td>
						<td><strong>Actions</strong></td>
					</tr>
				</thead>
				<tbody>
					<?php
						while ($item = mysqli_fetch_array($result)):
							if($item['Missing']){
								$row_class = "bg-danger";
							} else {
								$row_class = "bg-success";
							}
					?>
					<tr <?php echo "class = $row_class"; ?>>
						<td>
						<?php
							// extract date from datetime in IST
							$date = new DateTime($item['Time']);
							$date->add(new DateInterval('PT5H30M'));
							echo $date->format('d M, Y') . "\n";
						?></td>
						<td><?php echo $item['Name']; ?></td>
						<td><?php echo $item['Sex']; ?></td>
						<td><?php echo $item['Age']; ?></td>
						<td><?php echo $item['Phone']; ?></td>
						<td><?php echo $item['Location']; ?></td>
						<td><?php echo "<a href='update.php?id=$item[PersonID]' class='btn btn-light shadow' role='button''><i class='fas fa-pen'></i> Update</a>"; ?></td>
					</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		<!-- MDBootstrap Datatables  -->
		<script type="text/javascript" src="../js/datatables.min.js"></script>
		<script>
			$(document).ready(function () {
				$('#dtable').DataTable();
				$('.dataTables_length').addClass('bs-select');
			});
			$(".alert").delay(3000).slideUp(200, function() {
				$(this).alert('close');
			});
		</script>
	</body>

</html>

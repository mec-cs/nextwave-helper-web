<?php
	include('../session.php');

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}

	if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['action'] == 'Delete') {
		$name = mysqli_real_escape_string($db, $_POST['name']);
		$query = "DELETE FROM `missingpersons` WHERE `PersonID` = $id;";
		$result = mysqli_query($db, $query);
		if (!$result) {
			echo "Error Deleting!";
		} else {
			header('Location: index.php?del='.$name);
		}
		exit();
	}
	$grab_data_query = "SELECT Name, Sex, Age, Phone, Location, Missing FROM missingpersons WHERE PersonID = $id;";
	$data = mysqli_query($db, $grab_data_query);
	if (!$data) {
		echo "Error Fetching!";
		exit();
	}
	$person = mysqli_fetch_array($data);

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		if($_POST['action'] == 'Update') {
			$name = mysqli_real_escape_string($db, $_POST['name']);
			$age = mysqli_real_escape_string($db, $_POST['age']);
			$sex = mysqli_real_escape_string($db, $_POST['sex']);
			$phone = mysqli_real_escape_string($db, $_POST['phone']);
			$location = mysqli_real_escape_string($db, $_POST['loc']);
			$missing = mysqli_real_escape_string($db, $_POST['missing']);
			$query = "UPDATE `missingpersons` SET `Name`= '$name',`Sex`= '$sex',`Age`= '$age',`Phone`= '$phone',`Location`= '$location',`Missing`= '$missing' WHERE PersonID = $id;";
			$result = mysqli_query($db, $query);
			if (!$result) {
				$error = "Error Updating!";
			} else {
				$error = "Successfully Updated!";
				$data = mysqli_query($db, $grab_data_query);
				if (!$data) {
					echo "Error Fetching!";
					exit();
				}
				$person = mysqli_fetch_array($data);
			}
		}
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Missing Persons Database - Update Data</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<h3><a href = "index.php">⤺ Go Back</a></h3>
				</div>
				<div class="col text-center">
					<h3>Update Data : <B>'<?php echo $person['Name']; ?>'</B></h3>
				</div>
				<div class="col-lg-3">
					<h3 ALIGN=RIGHT><a href = "../logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-5 rounded border border-dark text-center" style="padding:20px; padding-right:30px;">
						<div class="form-group row justify-content-center">
							<?php
							if (isset($error))
								echo '<div class="alert bg-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									'.$error.'
								</div>';
							?>
						</div>
					<form method="POST" action="update.php?id=<?php echo $id; ?>">
						<div class="form-group row">
							<label class="col-3 col-form-label">Name</label>
							<input type="text" name="name" class="col form-control" value="<?php echo $person['Name']; ?>" required>
						</div>
						<div class="form-group row">
							<label class="col-3 col-form-label">Age</label>
							<input type="number" class="col form-control" name="age" min="1" value="<?php echo $person['Age']; ?>" required>
						</div>
						<div class="form-group row ">
							<label class="col-3 col-form-label">Sex :</label>
							<div class="col-3 custom-control custom-radio custom-control-inline">
								<input type="radio" id="maleRadio" name="sex" class="custom-control-input" value="M" <?php if($person['Sex'] == 'M') echo "checked"; ?> required>
								<label class="custom-control-label" for="maleRadio">Male</label>
							</div>
							<div class="col-3 custom-control custom-radio custom-control-inline">
								<input type="radio" id="femaleRadio" name="sex" class="custom-control-input" value="F" <?php if($person['Sex'] == 'F') echo "checked"; ?> required>
								<label class="custom-control-label" for="femaleRadio">Female</label>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-3 col-form-label">Phone</label>
							<input type="text" name="phone" class="col form-control" value="<?php echo $person['Phone']; ?>"
								   pattern="[0-9]{10}" oninvalid="this.setCustomValidity('Enter a valid phone number')"
								   oninput="this.setCustomValidity('')" required>
						</div>
						<div class="form-group row">
							<label class="col-3 col-form-label">Location</label>
							<input type="text" name="loc" class="col form-control" value="<?php echo $person['Location']; ?>" required>
						</div>
						<div class="form-group row">
							<label class="col-3 col-form-label">Status:</label>
							<div class="col-3 custom-control custom-radio custom-control-inline">
								<input type="radio" id="missing" name="missing" class="custom-control-input" value="1" <?php if($person['Missing']) echo "checked"; ?> required>
								<label class="custom-control-label" for="missing">Missing</label>
							</div>
							<div class="col-3 custom-control custom-radio custom-control-inline">
								<input type="radio" id="found" name="missing" class="custom-control-input" value="0" <?php if(!$person['Missing']) echo "checked"; ?> required>
								<label class="custom-control-label" for="found">Found</label>
							</div>
						</div>
						<button type="submit" class="col btn btn-primary" name="action" value="Update">Update</button>
						<button type="submit" class="col btn btn-danger" name="action" value="Delete">Delete Record</button>
					</form>
				</div>
			</div>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		<script>
			$(".alert").delay(2000).slideUp(200, function() {
				$(this).alert('close');
			});
		</script>
	</body>

</html>

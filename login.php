<?php
	include("config.php");
	session_start();
	$error = "";
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$myusername = mysqli_real_escape_string($db, $_POST['username']);
		$mypassword = hash('sha256', mysqli_real_escape_string($db, $_POST['password']));

		$sql = "SELECT * FROM adminusers WHERE username = '$myusername' and password = '$mypassword'";
		$result = mysqli_query($db, $sql);
		if(mysqli_num_rows($result) == 1) {
			$_SESSION['login_user'] = $myusername;
			header("location: index.php");
		}else {
			$error = "Username or Password is invalid";
		}
	}
?>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Kodinger">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>NextWave Helper Admin</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/my-login.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body class="my-login-page bg-dark">
	<section class="h-100">
		<div class="container h-100">
			<div class="row justify-content-md-center h-100">
				<div class="card-wrapper">
					<div class="brand">
						<img src="img/logo.jpg" alt="logo">
					</div>
					<div class="card fat bg-secondary text-white">
						<div class="card-body">
							<h4 class="card-title">Login</h4>
							<form method="POST" class="my-login-validation" novalidate="" action="">
								<div class="form-group">
									<label for="username">Username</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fas fa-user"></i>
											</div>
										</div>
										<input id="username" type="username" class="form-control" name="username" value="" required autofocus>
									</div>
								</div>

								<div class="form-group">
									<label for="password">Password</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<i class="fas fa-key"></i>
											</div>
										</div>
										<input id="password" type="password" class="form-control" name="password" required data-eye>
									</div>								
								</div>

								<div class="form-group m-0">
									<button type="submit" class="btn btn-primary btn-block">
										Login
									</button>
								</div>
								<div style = "font-size:12px; color:#cc0000; margin-top:10px; text-align:center;"><?php echo $error; ?></div>
							</form>
						</div>
					</div>
					<div class="footer">
						<b>NextWave Helper Web Portal</b>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/remove_banner.js"></script>
</body>
</html>
<?php
	include('session.php');
?>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>NextWave Helper Web Portal</title>

		<!-- Bootstrap core CSS -->
		<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h3>Welcome <?php echo $login_session; ?></h3>
				</div>
				<div class="col-lg-6">
					<h3 ALIGN=RIGHT><a href = "logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 text-center">
					<a href="missing_persons" class="tile gradient1" data-tilt data-tilt-glare="true" data-tilt-max-glare="0.8" data-tilt-speed="800" data-tilt-scale="1.1" data-tilt-max="15" data-tilt-perspective="500">
						<div class="inner">
							<img class="tile-logos" src="img/missing_person_logo.png">
							<br>
							<span>Missing Persons</span>
						</div>
					</a>
					<a href="announcements" class="tile gradient4" data-tilt data-tilt-glare="true" data-tilt-max-glare="0.8" data-tilt-speed="800" data-tilt-scale="1.1" data-tilt-max="15" data-tilt-perspective="500">
						<div class="inner">
							<img class="tile-logos" src="img/announcements.png">
							<br>
							<span>Announcements</span>
						</div>
					</a>
				</div>
				<div class="col-lg-4 text-center">
					<a href="relief_center_locations" class="tile gradient2" data-tilt data-tilt-glare="true" data-tilt-max-glare="0.8" data-tilt-speed="800" data-tilt-scale="1.1" data-tilt-max="15" data-tilt-perspective="500">
						<div class="inner">
							<img class="tile-logos" src="img/locations.png">
							<br>
							<span>Relief Center Locations</span>
						</div>
					</a>
					<a href="help_requests" class="tile gradient3" data-tilt data-tilt-glare="true" data-tilt-max-glare="0.8" data-tilt-speed="800" data-tilt-scale="1.1" data-tilt-max="15" data-tilt-perspective="500">
						<div class="inner">
							<img class="tile-logos" src="img/request.png">
							<br>
							<span>Help Requests</span>
						</div>
					</a>
				</div>

				<div class="col-lg-4 text-center">
					<a href="analytics" class="tile gradient5" data-tilt data-tilt-glare="true" data-tilt-max-glare="0.8" data-tilt-speed="800" data-tilt-scale="1.1" data-tilt-max="15" data-tilt-perspective="500">
						<div class="inner">
							<img class="tile-logos" src="img/analytics.png">
							<br>
							<span>Analytics</span>
						</div>
					</a>

					<a href="rescue_workers" class="tile gradient6" data-tilt data-tilt-glare="true" data-tilt-max-glare="0.8" data-tilt-speed="800" data-tilt-scale="1.1" data-tilt-max="15" data-tilt-perspective="500">
						<div class="inner">
							<img class="tile-logos" src="img/lifesaver.png">
							<br>
							<span>Rescue Workers</span>
						</div>
					</a>
				</div>
			</div>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="js/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="js/remove_banner.js"></script>
		<script type="text/javascript" src="js/vanilla-tilt.js"></script>
	</body>
</html>
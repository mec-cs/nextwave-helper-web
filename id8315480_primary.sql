-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 03, 2019 at 07:23 AM
-- Server version: 10.2.12-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id8315480_primary`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminusers`
--

CREATE TABLE `adminusers` (
  `AdminID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `adminusers`
--

INSERT INTO `adminusers` (`AdminID`, `Name`, `Username`, `Password`) VALUES
(1, 'Test User', 'root', 'CE5CA673D13B36118D54A7CF13AEB0CA012383BF771E713421B4D1FD841F539A');

-- --------------------------------------------------------

--
-- Table structure for table `affectedlocations`
--

CREATE TABLE `affectedlocations` (
  `MarkID` int(11) NOT NULL,
  `Longitude` double NOT NULL,
  `Latitude` double NOT NULL,
  `Radius` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `AnnouncementID` int(11) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ImportanceLevel` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`AnnouncementID`, `Time`, `Description`, `ImportanceLevel`) VALUES
(1, '2019-02-02 21:08:29', 'help', 1),
(2, '2019-02-02 21:08:55', 'dead', 0),
(3, '2019-02-02 21:09:11', 'HAAAAALLP', 2);

-- --------------------------------------------------------

--
-- Table structure for table `helprequests`
--

CREATE TABLE `helprequests` (
  `RequestID` int(11) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `Longitude` double NOT NULL,
  `Latitude` double NOT NULL,
  `Location` text COLLATE utf8_unicode_ci NOT NULL,
  `Needs` text COLLATE utf8_unicode_ci NOT NULL,
  `Status` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `missingpersons`
--

CREATE TABLE `missingpersons` (
  `PersonID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Sex` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `Age` int(11) NOT NULL,
  `Phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `Location` text COLLATE utf8_unicode_ci NOT NULL,
  `Missing` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `missingpersons`
--

INSERT INTO `missingpersons` (`PersonID`, `Name`, `Sex`, `Age`, `Phone`, `Location`, `Missing`) VALUES
(1, 'Anand Shekhar', 'M', 21, '1234567890', 'MEC, Kochi', 0),
(2, 'Saran Narayan', 'M', 69, '1234567899', 'MEC, Thrikakkasd', 1),
(3, 'Joe Davis', 'M', 21, '9988776655', 'Bakkar Hostel, Near MEC, Kochi', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reliefcamplocations`
--

CREATE TABLE `reliefcamplocations` (
  `CampID` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `Longitude` double NOT NULL,
  `Latitude` double NOT NULL,
  `Address` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reliefcamplocations`
--

INSERT INTO `reliefcamplocations` (`CampID`, `Name`, `Phone`, `Longitude`, `Latitude`, `Address`) VALUES
(1, 'JLN Stadium', '9988776655', 76.3008356220485, 9.99719035, 'Jawaharlal Nehru International Stadium, Stadium Road, Palarivattom, Kochi, Ernakulam, Kerala, 682025, India');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminusers`
--
ALTER TABLE `adminusers`
  ADD PRIMARY KEY (`AdminID`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- Indexes for table `affectedlocations`
--
ALTER TABLE `affectedlocations`
  ADD PRIMARY KEY (`MarkID`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`AnnouncementID`);

--
-- Indexes for table `helprequests`
--
ALTER TABLE `helprequests`
  ADD PRIMARY KEY (`RequestID`);

--
-- Indexes for table `missingpersons`
--
ALTER TABLE `missingpersons`
  ADD PRIMARY KEY (`PersonID`);

--
-- Indexes for table `reliefcamplocations`
--
ALTER TABLE `reliefcamplocations`
  ADD PRIMARY KEY (`CampID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminusers`
--
ALTER TABLE `adminusers`
  MODIFY `AdminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `affectedlocations`
--
ALTER TABLE `affectedlocations`
  MODIFY `MarkID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `AnnouncementID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `helprequests`
--
ALTER TABLE `helprequests`
  MODIFY `RequestID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `missingpersons`
--
ALTER TABLE `missingpersons`
  MODIFY `PersonID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reliefcamplocations`
--
ALTER TABLE `reliefcamplocations`
  MODIFY `CampID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

$app = require __DIR__.'/../../api/bootstrap/app.php';

$request = Illuminate\Http\Request::capture();
$app->run($request);
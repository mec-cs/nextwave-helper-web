<?php
	include('../session.php');

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}

	if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['action'] == 'Delete') {
		$name = mysqli_real_escape_string($db, $_POST['name']);
		$query = "DELETE FROM `reliefcamplocations` WHERE `CampID` = $id;";
		$result = mysqli_query($db, $query);
		if (!$result) {
			echo "Error Deleting!";
		} else {
			header('Location: index.php?del='.$name);
		}
		exit();
	}
	$grab_data_query = "SELECT  CampID, Name, Phone, Longitude, Latitude, Address FROM reliefcamplocations WHERE CampID = $id;";
	$data = mysqli_query($db, $grab_data_query);
	if (!$data) {
		echo "Error Fetching!";
		exit();
	}
	$camp = mysqli_fetch_array($data);

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		if($_POST['action'] == 'Update') {
			$name = mysqli_real_escape_string($db, $_POST['name']);
			$address = mysqli_real_escape_string($db, $_POST['adr']);
			$phone = mysqli_real_escape_string($db, $_POST['phone']);
			$lat = mysqli_real_escape_string($db, $_POST['lat']);
			$lng = mysqli_real_escape_string($db, $_POST['lng']);

			$query = "UPDATE `reliefcamplocations` SET `Name`= '$name',`Address`= '$address', `Phone`= '$phone',`Longitude`= $lng,`Latitude`= $lat WHERE campID = $id;";
			$result = mysqli_query($db, $query);
			if (!$result) {
				$error = "Error Updating!";
			} else {
				$error = "Successfully Updated!";
				$data = mysqli_query($db, $grab_data_query);
				if (!$data) {
					echo "Error Fetching!";
					exit();
				}
				$camp = mysqli_fetch_array($data);
			}
		}
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Relief Center Locations Database - Update Data</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="css/leaflet.css">
		<script src="../js/leaflet.js"></script>
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<h3><a href = "index.php">⤺ Go Back</a></h3>
				</div>
				<div class="col text-center">
					<h3>Update Data : <B>'<?php echo $camp['Name']; ?>'</B></h3>
				</div>
				<div class="col-lg-3">
					<h3 ALIGN=RIGHT><a href = "../logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-5 rounded border border-dark text-center" style="padding:20px; padding-right:30px;">
						<div class="form-group row justify-content-center">
							<?php
							if (isset($error))
								echo '<div class="alert bg-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									'.$error.'
								</div>';
							?>
						</div>
					<form method="POST" action="update.php?id=<?php echo $id; ?>">
						<div class="form-group row">
							<label class="col-3 col-form-label">Name</label>
							<input type="text" name="name" class="col form-control" value="<?php echo $camp['Name']; ?>" required>
						</div>
						<div class="form-group row">
							<label class="col-3 col-form-label">Phone</label>
							<input type="text" name="phone" class="col form-control" value="<?php echo $camp['Phone']; ?>"
								   pattern="[0-9]{10}" oninvalid="this.setCustomValidity('Enter a valid phone number')"
								   oninput="this.setCustomValidity('')" required>
						</div>
						<div class="form-group row">
								<label class="col-3 col-form-label">Address</label>
								<input type="text" id="adr" name="adr" class="col form-control" value="<?php echo $camp['Address']; ?>" placeholder= "Enter road name to search" required>
								<span class="input-group-btn">
									<button class="btn btn-success" id="search" type="button" onclick="searchMap();">
										<i class="fa fa-search"></i>
									</button>
								</span>
						</div>
						<div class="form-group row">
							<label class="col-3 col-form-label">Latitude</label>
							<input type="text" name="lat" id="lat" class="col form-control" value="<?php echo $camp['Latitude']; ?>" placeholder="Click on the map" required>
						</div>
						<div class="form-group row">
							<label class="col-3 col-form-label">Longitude</label>
							<input type="text" name="lng"  id="lng" class="col form-control" value="<?php echo $camp['Longitude']; ?>" placeholder="Click on the map" required>
						</div>
						<button type="submit" class="col btn btn-primary" name="action" value="Update">Update</button>
						<button type="submit" class="col btn btn-danger" name="action" value="Delete">Delete Record</button>
					</form>
				</div>
			</div>

			<div id="map"></div>

		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>

		<script>
			var lat_input = document.getElementById("lat");
			var lng_input = document.getElementById("lng");
			var curr_loc = L.latLng(lat_input.value, lng_input.value);
			var map = L.map('map').setView(curr_loc, 15);
			var adr_input = document.getElementById("adr");
			map.on('click', onMapClick);
			var marker = L.marker(curr_loc).addTo(map);

			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1IjoibmV4dHdhdmVoZWxwZXIiLCJhIjoiY2pybmxwOHFiMHU2bzQ0dGtjN2x1Mjl3ZCJ9.fd5KPogARWQAoX1KtnwZoA'
			}).addTo(map);

			function onMapClick(e) {
				placeMarker(e.latlng);
			}
			function placeMarker(latlng){
				marker.setLatLng(latlng);
				map.panTo(latlng);
				lat_input.value = latlng.lat;
				lng_input.value = latlng.lng;
				var url = "https://nominatim.openstreetmap.org/reverse?format=json&lat="+latlng.lat+"&lon="+latlng.lng+"&format=json";
				$.getJSON(url, function (result) {
					if (result.length == 0) {
						return;
					}
					var popupContent = "<b>"+result.display_name+"</b> <br> <button class='btn btn-success' id='search' type='button' onclick='useLoc(\""+result.display_name+"\");'>Use This Address</button>";
					marker.bindPopup(popupContent).openPopup();
					console.log(popupContent);
				});
			}
			function searchMap(){
				var url = "https://nominatim.openstreetmap.org/search?q="+adr_input.value+"&format=json&limit=1";
				$.getJSON(url, function (result) {
					if (result.length == 0) {
						$('#search').addClass('btn-danger');
						return;
					}
					$('#search').removeClass('btn-danger');
					var loc = L.latLng(result[0].lat, result[0].lon);

					$('html,body').animate({scrollTop: document.body.scrollHeight},"slow");
					placeMarker(loc);
					map.flyTo(loc, 15);
				});
			}
			function useLoc(adr){
				adr_input.value = adr;
			}
		</script>
		<script>
			$(".alert").delay(2000).slideUp(200, function() {
				$(this).alert('close');
			});
		</script>
	</body>

</html>

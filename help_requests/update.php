<?php
	include('../session.php');

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}

	if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['action'] == 'Delete') {
		$query = "DELETE FROM `helprequests` WHERE `RequestID` = $id;";
		$result = mysqli_query($db, $query);
		if (!$result) {
			echo "Error Deleting!";
		} else {
			header('Location: index.php?del='.$id);
		}
		exit();
	}
	$grab_data_query = "SELECT `RequestID`, `Status`, `WorkerName`, `WorkerNumber`, `Longitude`, `Latitude` FROM `helprequests` WHERE RequestID = $id;";
	$data = mysqli_query($db, $grab_data_query);
	if (!$data) {
		echo "Error Fetching!";
		exit();
	}
	$request = mysqli_fetch_array($data);

	$rescueWorkers = "SELECT * FROM `rescueworkers` WHERE HelpRequestID IS NULL;";
	$rescueWorkersData = mysqli_query($db, $rescueWorkers);
	if (!$rescueWorkersData) {
		echo "Error Fetching!";
		exit();
	}

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		if($_POST['action'] == 'Update') {
			$oldWorkerNumber = $request['WorkerNumber'];
			$status = mysqli_real_escape_string($db, $_POST['status']);
			$workername = mysqli_real_escape_string($db, $_POST['workerName']);
			$workernumber = mysqli_real_escape_string($db, $_POST['workerNumber']);
			// Set HelpRequestID for the RescueWorkers if it is inprogress
			if($status == 1){
				$updateRescueWorkersQuery = "UPDATE `rescueworkers` SET `HelpRequestID`= '$id' WHERE `WorkerPhone`='$workernumber';";
				$updateRescueWorkersQueryResult = mysqli_query($db, $updateRescueWorkersQuery);
			} else {
				$updateRescueWorkersQuery = "UPDATE `rescueworkers` SET `HelpRequestID`= NULL WHERE `WorkerPhone`='$workernumber';";
				$updateRescueWorkersQueryResult = mysqli_query($db, $updateRescueWorkersQuery);
			}

			// Unassign the previous rescue worker if any.
			if($oldWorkerNumber != ""){
				$updateRescueWorkersQuery = "UPDATE `rescueworkers` SET `HelpRequestID`= NULL WHERE `WorkerPhone`='$oldWorkerNumber';";
				$updateRescueWorkersQueryResult = mysqli_query($db, $updateRescueWorkersQuery);
			}

			// Resetting the worker info if status is 2 which is unsolved.
			if($status == 2){
				$workername = NULL;
				$workernumber = NULL;
			}
			$updateHelpRequestQuery = "UPDATE `helprequests` SET `Status`='$status', `WorkerName`='$workername', `WorkerNumber`='$workernumber' WHERE RequestID = $id;";
			$result = mysqli_query($db, $updateHelpRequestQuery);
			if (!$result) {
				$error = "Error Updating!";
			} else {
				$error = "Successfully Updated!";
				$data = mysqli_query($db, $grab_data_query);
				if (!$data) {
					echo "Error Fetching!";
					exit();
				}
				$rescueWorkersData = mysqli_query($db, $rescueWorkers);
				if (!$rescueWorkersData) {
					echo "Error Fetching!";
					exit();
				}
				$request = mysqli_fetch_array($data);
			}
		}
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Help Requests - Update Data</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

		<!-- Leaflet Maps -->
		<link rel="stylesheet" href="css/leaflet.css">
		<link rel="stylesheet" href="css/MarkerCluster.css">
		<link rel="stylesheet" href="css/MarkerCluster.Default.css">
		<script src="../js/leaflet.js"></script>
		<script src="../js/leaflet.markercluster.js"></script>
		
		<!-- Time related stuff -->
		<script src="../js/moment.min.js"></script>
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<h3><a href = "index.php">⤺ Go Back</a></h3>
				</div>
				<div class="col text-center">
				</div>
					<div class="col-lg-3">
					<h3 ALIGN=RIGHT ><a href = "../logout.php">Sign Out</a></h3>
					</div>
				</div>


			<div class="row justify-content-center">
				<div class="col-lg-6 rounded border border-dark text-center" style="padding:20px; padding-right:30px;">
						<div class="form-group row justify-content-center">
							<?php
							if (isset($error))
								echo '<div class="alert bg-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									'.$error.'
								</div>';
							?>
						</div>
					<form method="POST" action="update.php?id=<?php echo $id; ?>">
						<div class="form-group row">
							<label class="col-3 col-form-label">Status:</label>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="unsolvedstatus" name="status" class="custom-control-input" value="2" <?php if($request['Status']==2) echo "checked"; ?> required>
								<label class="custom-control-label" for="unsolvedstatus">Unsolved</label>
							</div>
							<div class="col-3 custom-control custom-radio custom-control-inline">
								<input type="radio" id="inprogress" name="status" class="custom-control-input" value="1" <?php if($request['Status']==1) echo "checked"; ?> required>
								<label class="custom-control-label" for="inprogress">In-progress</label>
							</div>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="solvedstatus" name="status" class="custom-control-input" value="0" <?php if($request['Status']==0) echo "checked"; ?> required>
								<label class="custom-control-label" for="solvedstatus">Solved</label>
							</div>
							<div class="form-group row">
								<label class="col-6 col-form-label">Worker Name</label>
								<input type="text" id="workerName" name="workerName" class="col form-control" value="<?php echo $request['WorkerName']; ?>" required>
							</div>
							<div class="form-group row">
								<label class="col-6 col-form-label">Worker Number</label>
								<input type="text" id="workerPhone" name="workerNumber" class="col form-control" value="<?php echo $request['WorkerNumber']; ?>"
								   pattern="[0-9]{10}" oninvalid="this.setCustomValidity('Enter a valid phone number')"
								   oninput="this.setCustomValidity('')" required>
							</div>
						</div>
						<button type="submit" class="col btn btn-primary" name="action" value="Update">Update</button>
					</form>
					<form method="POST" action="update.php?id=<?php echo $id; ?>">
						<button type="submit" class="col btn btn-danger" name="action" value="Delete">Delete Record</button>
					</form>
				</div>

				<div class="maps" id="rescueWorkersMap" style="margin-top: 20px"></div>

			</div>

		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		<script>
			$(".alert").delay(2000).slideUp(200, function() {
				$(this).alert('close');
			});
		</script>

		<script>
			var redIcon = new L.Icon({
				iconUrl: 'css/images/marker-icon-2x-red.png',
				shadowUrl: 'css/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});

			var greenIcon = new L.Icon({
				iconUrl: 'css/images/marker-icon-2x-green.png',
				shadowUrl: 'css/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});

			var requestLoc = L.latLng(<?php echo $request['Latitude']; ?>, <?php echo $request['Longitude'] ?>);
			var rescueWorkersMap = L.map('rescueWorkersMap').setView(requestLoc, 15);
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1IjoibmV4dHdhdmVoZWxwZXIiLCJhIjoiY2pybmxwOHFiMHU2bzQ0dGtjN2x1Mjl3ZCJ9.fd5KPogARWQAoX1KtnwZoA'
			}).addTo(rescueWorkersMap);

			// Help Request Marker.
			var helpRequestMarker = L.marker(requestLoc, {icon: redIcon});
			rescueWorkersMap.addLayer(helpRequestMarker);

			var markersRescueWorker = L.markerClusterGroup();
			var rescueWorkers = <?php echo json_encode(mysqli_fetch_all($rescueWorkersData, MYSQLI_ASSOC)); ?>;
			for (var i = 0; i < rescueWorkers.length; i++) {
				var worker = rescueWorkers[i];
				var marker = L.marker(new L.LatLng(worker['Latitude'], worker['Longitude']), {icon: greenIcon});
				
				var popupContent = "<b> Name: </b>"+worker['WorkerName']
									+" <br> <b>Phone: </b>"+worker['WorkerPhone']
									+"<br> <button class='btn btn-sm btn-danger' type='button' onclick='assignWorker(\""+worker['WorkerName']+"\", \""+worker['WorkerPhone']+"\")'>Assign to request</button>";				
				marker.bindPopup(popupContent);
				var timeStamp = new Date(worker['UpdatedAt']);
				// Converting to IST
				timeStamp.setHours( timeStamp.getHours() + 5);
                timeStamp.setMinutes( timeStamp.getMinutes() + 30);
				marker.bindTooltip("<b>Updated: </b>" + moment(timeStamp).fromNow(), {direction:'bottom'});
				markersRescueWorker.addLayer(marker);
			}
			rescueWorkersMap.addLayer(markersRescueWorker);

			function assignWorker(workerName, workerPhone){
				var worker_name_input = document.getElementById("workerName");
				var worker_phone_input = document.getElementById("workerPhone");
				var inprogress_radio_btn = document.getElementById("inprogress");
				worker_name_input.value = workerName;
				worker_phone_input.value = workerPhone;
				inprogress_radio_btn.checked = true;
				$('body').animate({scrollTop:0}, 500, 'swing');
			}
		</script>
	</body>

</html>

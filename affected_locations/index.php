<?php
    include('../config.php');
    $query = "SELECT * FROM affectedlocations;";
	$result = mysqli_query($db, $query);
    if (!$result) {
		echo "Error Fetching!";
		exit();
    }
    $geoJson = new \stdClass();
    $geoJson->type = "FeatureCollection";
    $geoJson->features = array();
        
    while ($affectedLocation = mysqli_fetch_assoc($result)){
        array_push($geoJson->features, json_decode($affectedLocation['Feature']));
    }

    header("Content-type:application/json");
    echo json_encode($geoJson);
?>
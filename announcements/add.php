<?php
	include('../session.php');
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$desc = mysqli_real_escape_string($db, $_POST['Description']);
		$importance = mysqli_real_escape_string($db, $_POST['ImportanceLevel']);
		$query = "INSERT INTO `announcements`(`Description`, `ImportanceLevel`) VALUES ('$desc','$importance');";
		$result = mysqli_query($db, $query);
		if (!$result) {
			$error = "Error Adding!";
		} else {
			$error = "Successfully Added!";
		}
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Announcements - Add New Entry</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<h3><a href = "index.php">⤺ Go Back</a></h3>
				</div>
				<div class="col text-center">
					<h3><B>Add New Announcement</B></h3>
				</div>
				<div class="col-lg-3">
					<h3 ALIGN=RIGHT><a href = "../logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-7 rounded border border-dark text-center" style="padding:20px; padding-right:30px;">
						<div class="form-group row justify-content-center">
							<?php
							if (isset($error))
								echo '<div class="alert bg-success alert-dismissible">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										'.$error.'
									</div>';
							?>
						</div>
					<form method="POST" action="add.php">
						<div class="form-group row">
							<label class="col-3 col-form-label">Description:</label>
							<textarea class="col form-control" name="Description" rows="4" required></textarea>
						</div>
						<div class="form-group row ">
							<label class="col-3 col-form-label">Importance Level:</label>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="highRadio" name="ImportanceLevel" class="custom-control-input" value="2" required>
								<label class="custom-control-label" for="highRadio">High</label>
							</div>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="medRadio" name="ImportanceLevel" class="custom-control-input" value="1" required>
								<label class="custom-control-label" for="medRadio">Medium</label>
							</div>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="lowRadio" name="ImportanceLevel" class="custom-control-input" value="0" required>
								<label class="custom-control-label" for="lowRadio">Low</label>
							</div>
						</div>
						<button type="submit" class="col btn btn-primary">Add</button>
					</form>
				</div>
			</div>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		<script>
			$(".alert").delay(2000).slideUp(200, function() {
				$(this).alert('close');
			});
		</script>
	</body>

</html>

<?php
	include('../session.php');

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	}

	if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['action'] == 'Delete') {
		$query = "DELETE FROM `announcements` WHERE `AnnouncementID` = $id;";
		$result = mysqli_query($db, $query);
		if (!$result) {
			echo "Error Deleting!";
		} else {
			header('Location: index.php?del='.$id);
		}
		exit();
	}
	$grab_data_query = "SELECT Description,ImportanceLevel FROM announcements WHERE AnnouncementID = $id;";
	$data = mysqli_query($db, $grab_data_query);
	if (!$data) {
		echo "Error Fetching!";
		exit();
	}
	$announcement = mysqli_fetch_array($data);

	if($_SERVER["REQUEST_METHOD"] == "POST") {

		if($_POST['action'] == 'Update') {
			$desc = mysqli_real_escape_string($db, $_POST['Description']);
			$importance = mysqli_real_escape_string($db, $_POST['ImportanceLevel']);
			$query = "UPDATE `announcements` SET `Description`='$desc',`ImportanceLevel`='$importance' WHERE AnnouncementID = $id";
			$result = mysqli_query($db, $query);
			if (!$result) {
				$error = "Error Updating!";
			} else {
				$error = "Successfully Updated!";
				$data = mysqli_query($db, $grab_data_query);
				if (!$data) {
					echo "Error Fetching!";
					exit();
				}
				$announcement = mysqli_fetch_array($data);
			}
		}
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Announcements - Update Data</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<h3><a href = "index.php">⤺ Go Back</a></h3>
				</div>
				<div class="col text-center">
					<h3>Update Data</h3>
				</div>
				<div class="col-lg-3">
					<h3 ALIGN=RIGHT><a href = "../logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-lg-7 rounded border border-dark text-center" style="padding:20px; padding-right:30px;">
						<div class="form-group row justify-content-center">
							<?php
							if (isset($error))
								echo '<div class="alert bg-success alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									'.$error.'
								</div>';
							?>
						</div>
					<form method="POST" action="update.php?id=<?php echo $id; ?>">
						<div class="form-group row">
							<label class="col-3 col-form-label">Description:</label>
							<textarea class="col form-control" name="Description" rows="4" required><?php echo $announcement['Description']; ?></textarea>
						</div>
						<div class="form-group row ">
							<label class="col-3 col-form-label">Importance Level:</label>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="highRadio" name="ImportanceLevel" class="custom-control-input" value="2" <?php if($announcement['ImportanceLevel'] == '2') echo "checked"; ?> required>
								<label class="custom-control-label" for="highRadio">High</label>
							</div>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="medRadio" name="ImportanceLevel" class="custom-control-input" value="1" <?php if($announcement['ImportanceLevel'] == '1') echo "checked"; ?> required>
								<label class="custom-control-label" for="medRadio">Medium</label>
							</div>
							<div class="col-2 custom-control custom-radio custom-control-inline">
								<input type="radio" id="lowRadio" name="ImportanceLevel" class="custom-control-input" value="0" <?php if($announcement['ImportanceLevel'] == '0') echo "checked"; ?> required>
								<label class="custom-control-label" for="lowRadio">Low</label>
							</div>
						</div>
						<button type="submit" class="col btn btn-primary" name="action" value="Update">Update</button>
						<button type="submit" class="col btn btn-danger" name="action" value="Delete">Delete Record</button>
					</form>
				</div>
			</div>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		<script>
			$(".alert").delay(2000).slideUp(200, function() {
				$(this).alert('close');
			});
		</script>
	</body>

</html>

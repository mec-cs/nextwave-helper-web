<?php
	include('../session.php');

	$operationID = 1;

	if (isset($_GET['id'])) {
		$operationID = $_GET['id'];
	}

	$operationStartDate = "(SELECT OperationStartDate FROM archivedata WHERE OperationID = $operationID)";
	$operationEndDate = "(SELECT OperationEndDate FROM archivedata WHERE OperationID = $operationID)";

	$reliefCampListQuery = "SELECT  CampID, Name, Phone, Longitude, Latitude, Address
							FROM reliefcamplocations
							WHERE Time >= $operationStartDate AND
							(Time <= $operationEndDate OR $operationEndDate IS NULL);";
	$reliefCampResult = mysqli_query($db, $reliefCampListQuery);
	if (!$reliefCampResult) {
		echo "Error Fetching Relief Camps!";
		exit();
	}
	$reliefCampCoordinateData = array();
	while($reliefCamp = mysqli_fetch_array($reliefCampResult)){
		$reliefCampCoordinateData[] = array($reliefCamp['Latitude'], $reliefCamp['Longitude']);
	}

	$missingPersonListQuery = "SELECT PersonID, Time, Name, Sex, Age, Phone, Location, Missing
							   FROM missingpersons
							   WHERE Time >= $operationStartDate AND
							   (Time <= $operationEndDate OR $operationEndDate IS NULL)
							   ORDER BY Missing DESC;";
	$missingPersonResult = mysqli_query($db, $missingPersonListQuery);
	if (!$missingPersonResult) {
		echo "Error Fetching Missing Persons!";
		exit();
	}

	$missingPersonStatusData = array(0, 0); // missing, found
	$missingPersonAgeData = array(0, 0, 0, 0); // 1-10, 11-20, 21-45, 46+
	$missingPersonMapData = array();
	while($missingPerson = mysqli_fetch_array($missingPersonResult)){
		$personAge = $missingPerson['Age'];
		if($missingPerson['Missing']){
			$missingPersonStatusData[0]++;
		} else {
			$missingPersonStatusData[1]++;
		}

		$missingPersonMapData[] = array($missingPerson['Location']);

		if($personAge <= 10) $missingPersonAgeData[0]++;
		else if ($personAge <= 20) $missingPersonAgeData[1]++;
		else if ($personAge <= 45) $missingPersonAgeData[2]++;
		else $missingPersonAgeData[3]++;
	}

	$helpRequestListQuery = "SELECT RequestID, Time, Name, Phone, Longitude, Latitude, Location, Needs, Status, WorkerName, WorkerNumber
							 FROM helprequests
							 WHERE Time >= $operationStartDate AND
							 (Time <= $operationEndDate OR $operationEndDate IS NULL)
							 ORDER BY Time DESC;";
	$helpRequestResult = mysqli_query($db, $helpRequestListQuery);
	if (!$helpRequestResult) {
		echo "Error Fetching Help Requests!";
		exit();
	}
	$helpRequestStatusData = array(0,0,0); //unsolved, in-progress, solved.
	$helpRequestNeedsData = array(0,0,0,0); // Clothes, Food, Medicines, Others.
	$helpRequestCoordinateData = array();
	while ($helpRequest = mysqli_fetch_array($helpRequestResult)){
		$needs = array_map('trim',explode(",", $helpRequest['Needs']));
		foreach ($needs as $need) {
			if($need == "Clothes")
				$helpRequestNeedsData[0]++;
			else if($need == "Food")
				$helpRequestNeedsData[1]++;
			else if($need == "Medicines")
				$helpRequestNeedsData[2]++;
			else
				$helpRequestNeedsData[3]++;
		}

		// Help Requests Coordinates
		$helpRequestCoordinateData[] = array($helpRequest['Latitude'], $helpRequest['Longitude'], $helpRequest['Status']);

		if($helpRequest['Status']==0)
			$helpRequestStatusData[2]++; //solved
		else if($helpRequest['Status']==1)
			$helpRequestStatusData[1]++; //in-progress
		else
			$helpRequestStatusData[0]++; //unsolved
	}
	
	$announcementsData = array(0,0,0);
	$announcementsListQuery = "SELECT AnnouncementID, Time, Description, ImportanceLevel
							   FROM announcements
							   WHERE Time >= $operationStartDate AND
							   (Time <= $operationEndDate OR $operationEndDate IS NULL)
							   ORDER BY Time DESC;";

	$announcementsResult = mysqli_query($db, $announcementsListQuery);
	if (!$announcementsResult) {
		echo "Error Fetching Announcements!";
		exit();
	}

	while ($announcement = mysqli_fetch_array($announcementsResult)){
		if($announcement['ImportanceLevel']==0)
			$announcementsData[2]++;
		else if($announcement['ImportanceLevel']==1)
			$announcementsData[1]++;
		else
			$announcementsData[0]++;
	}

	$operationDetailsQuery = "SELECT OperationID, OperationName, OperationStartDate, OperationEndDate FROM archivedata WHERE OperationID = $operationID;";
	$operationDetailsResult = mysqli_query($db, $operationDetailsQuery);
	if (!$operationDetailsResult) {
		echo "Error Fetching!";
		exit();
	}
	$operationDetails = mysqli_fetch_array($operationDetailsResult)
?>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Analytics | Details</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<!-- MDBootstrap Datatables  -->
		<link href="../bootstrap/css/datatables.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/leaflet.css">
		<link rel="stylesheet" href="css/MarkerCluster.css">
		<link rel="stylesheet" href="css/MarkerCluster.Default.css">
		<script src="../js/leaflet.js"></script>
		<script src="../js/leaflet.markercluster.js"></script>
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
				<div class="col">
					<h3><a href = "index.php">⤺ Go Back</a></h3>
				</div>
				<div class="col-6 text-center">
					<span class="badge badge-dark">Operation Name: <?php echo $operationDetails['OperationName']; ?></span>
					<span class="badge badge-dark">Start Date: <?php echo $operationDetails['OperationStartDate']; ?></span>
					<span class="badge badge-dark">End Date: <?php echo $operationDetails['OperationEndDate']; ?></span>
				</div>
				<div class="col">
					<h3 ALIGN=RIGHT><a href = "../logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>
				</div>
			</div>

			<ul class="nav nav nav-pills mb-3" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="pills-announcements-tab" data-toggle="pill" href="#pills-announcements" role="tab" aria-controls="pills-announcements" aria-selected="true">Announcements</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-helpRequests-tab" data-toggle="pill" href="#pills-helpRequests" role="tab" aria-controls="pills-helpRequests" aria-selected="false">Help Requests</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-missingPersons-tab" data-toggle="pill" href="#pills-missingPersons" role="tab" aria-controls="pills-missingPersons" aria-selected="false">Missing Persons</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-reliefCenterLocations-tab" data-toggle="pill" href="#pills-reliefCenterLocations" role="tab" aria-controls="pills-reliefCenterLocations" aria-selected="false">Relief Center Locations</a>
				</li>
			</ul>

			<div class="tab-content" id="pills-tabContent">

				<div class="tab-pane fade show active" id="pills-announcements" role="tabpanel" aria-labelledby="pills-announcements-tab">
					<div class="row">
						<canvas id="announcementsChart" style="height: 300px; width: 100%;"></canvas>
					</div>
					<div class="text-right">
						<span>Level of danger: </span>
						<span class='badge badge-danger'> High </span>
						<span class='badge badge-warning'> Medium </span>
						<span class='badge badge-success'> Low </span>
					</div>
					<table id="dtable-announcement" class="mytable table-dark table-striped table" width="100%">
						<thead>
							<tr>
								<td style="width:15%"><strong>Date</strong></td>
								<td style="width:85%"><strong>Description</strong></td>
							</tr>
						</thead>
						<tbody>
							<?php
								mysqli_data_seek($announcementsResult, 0);
								while ($announcement = mysqli_fetch_array($announcementsResult)):
									if($announcement['ImportanceLevel']==0)
										$row_class = "bg-success";
									else if($announcement['ImportanceLevel']==1)
										$row_class = "bg-warning";
									else
										$row_class = "bg-danger";
							?>
							<tr <?php echo "class = $row_class"; ?>>
								<td>
								<?php
									// extract date from datetime in IST
									$date = new DateTime($announcement['Time']);
									$date->add(new DateInterval('PT5H30M'));
									echo $date->format('d M, Y') . "\n";
								?></td>
								<td><?php echo $announcement['Description']; ?></td>
							</tr>
							<?php endwhile; ?>
						</tbody>
					</table>
				</div>

				<div class="tab-pane fade" id="pills-helpRequests" role="tabpanel" aria-labelledby="pills-helpRequests-tab">
					<div class="row" style="margin-bottom: 50px">
						<div class="col">
							<canvas id="helpRequestsStatusChart"></canvas>
						</div>
						<div class="col">
							<canvas id="helpRequestsNeedsChart"></canvas>
						</div>
					</div>

					<div class="maps" id="helpRequestMap"></div>

					<div class="row">
						<div class="col text-right">
							<span>Status: </span>
							<span class='badge badge-danger'> Unsolved </span>
							<span class='badge badge-warning'> In Progress </span>
							<span class='badge badge-success'> Solved </span>
						</div>
					</div>
					<table id="dtable-helpreq" class="mytable table-dark table-striped table" width="100%">
						<thead>
							<tr>
								<td><strong>Time</strong></td>
								<td><strong>Name</strong></td>
								<td><strong>Phone</strong></td>
								<td><strong>Coordinates</strong></td>
								<td><strong>Location</strong></td>
								<td><strong>Needs</strong></td>
								<td><strong>Worker Name</strong></td>
								<td><strong>Worker Number</strong></td>
							</tr>
						</thead>
						<tbody>
							<?php
								mysqli_data_seek($helpRequestResult, 0);
								while ($helpRequest = mysqli_fetch_array($helpRequestResult)):
									if($helpRequest['Status']==0)
										$row_class = "bg-success";
									else if($helpRequest['Status']==1)
										$row_class = "bg-warning";
									else
										$row_class = "bg-danger";
							?>
							<tr <?php echo "class = $row_class"; ?>>
								<td>
								<?php
									// extract date and time in IST from datetime
									$date = new DateTime($helpRequest['Time']);
									$date->add(new DateInterval('PT5H30M'));
									echo $date->format('h:i A, d M, Y') . "\n";
								?></td>
								<td><?php echo $helpRequest['Name']; ?></td>
								<td><?php echo $helpRequest['Phone']; ?></td>
								<td><?php echo "<a target='_blank' href='https://www.google.com/maps/search/?api=1&query=$helpRequest[Latitude],$helpRequest[Longitude]' class='btn btn-light shadow' role='button''><i class='fas fa-external-link-alt'></i> Map</a>"; ?></td>
								<td><?php echo $helpRequest['Location']; ?></td>
								<td><?php echo $helpRequest['Needs']; ?></td>
								<td><?php echo $helpRequest['WorkerName']; ?></td>
								<td><?php echo $helpRequest['WorkerNumber']; ?></td>
							</tr>
							<?php endwhile; ?>
						</tbody>
					</table>
				</div>

				<div class="tab-pane fade" id="pills-missingPersons" role="tabpanel" aria-labelledby="pills-missingPersons-tab">
					<div class="row" style="margin-bottom: 50px">
						<div class="col">
							<canvas id="missingPersonsStatusChart"></canvas>
						</div>
						<div class="col">
							<canvas id="missingPersonsAgeChart"></canvas>
						</div>

					<div class="maps" id="missingPersonMap"></div>

					</div>
					<div class="text-right">
						<span>Legend: </span>
						<span class='badge badge-danger'> Missing </span>
						<span class='badge badge-success'> Found </span>
					</div>
					<table id="dtable-missing-persons" class="mytable table-dark table-striped table" width="100%">
						<thead>
							<tr>
								<td><strong>Date Reported</strong></td>
								<td><strong>Name</strong></td>
								<td><strong>Sex</strong></td>
								<td><strong>Age</strong></td>
								<td><strong>Phone</strong></td>
								<td><strong>Location</strong></td>
							</tr>
						</thead>
						<tbody>
							<?php
								mysqli_data_seek($missingPersonResult, 0);
								while ($missingPerson = mysqli_fetch_array($missingPersonResult)):
									if($missingPerson['Missing']){
										$row_class = "bg-danger";
									} else {
										$row_class = "bg-success";
									}
							?>
							<tr <?php echo "class = $row_class"; ?>>
								<td>
								<?php
									// extract date from datetime in IST
									$date = new DateTime($missingPerson['Time']);
									$date->add(new DateInterval('PT5H30M'));
									echo $date->format('d M, Y') . "\n";
								?></td>
								<td><?php echo $missingPerson['Name']; ?></td>
								<td><?php echo $missingPerson['Sex']; ?></td>
								<td><?php echo $missingPerson['Age']; ?></td>
								<td><?php echo $missingPerson['Phone']; ?></td>
								<td><?php echo $missingPerson['Location']; ?></td>
							</tr>
							<?php endwhile; ?>
						</tbody>
					</table>
				</div>

				<div class="tab-pane fade" id="pills-reliefCenterLocations" role="tabpanel" aria-labelledby="pills-reliefCenterLocations-tab">
					<div class="maps" id="reliefCampMap"></div>

					<table id="dtable-relief-center-loc" class="mytable table-dark table-striped table" width="100%">
						<thead>
							<tr>
								<td><strong>Name</strong></td>
								<td><strong>Phone</strong></td>
								<td><strong>Coordinates</strong></td>
								<td><strong>Address</strong></td>
							</tr>
						</thead>
						<tbody>
							<?php
								mysqli_data_seek($reliefCampResult, 0);
								while ($reliefCamp = mysqli_fetch_array($reliefCampResult)):
							?>
							<tr>
								<td><?php echo $reliefCamp['Name']; ?></td>
								<td><?php echo $reliefCamp['Phone']; ?></td>
								<td><?php echo "<a target='_blank' href='https://www.google.com/maps/search/?api=1&query=$reliefCamp[Latitude],$reliefCamp[Longitude]' class='btn btn-light shadow' role='button''><i class='fas fa-external-link-alt'></i> Map</a>"; ?></td>
								<td><?php echo $reliefCamp['Address']; ?></td>
							</tr>
							<?php endwhile; ?>
						</tbody>
					</table>
				</div>

			</div>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		
		<!-- MDBootstrap Datatables  -->
		<script type="text/javascript" src="../js/datatables.min.js"></script>

		<!-- Chart JS -->
		<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
		<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-deferred@1"></script>

		<script>
			$(document).ready(function () {
				$('#dtable-announcement').DataTable();
				$('#dtable-helpreq').DataTable();
				$('#dtable-missing-persons').DataTable();
				$('#dtable-relief-center-loc').DataTable();
				$('.dataTables_length').addClass('bs-select');
			});
		</script>

		<script>
			var style = getComputedStyle(document.body);
			var theme = {};
			theme.primary = style.getPropertyValue('--primary');
			theme.secondary = style.getPropertyValue('--secondary');
			theme.success = style.getPropertyValue('--success');
			theme.info = style.getPropertyValue('--info');
			theme.warning = style.getPropertyValue('--warning');
			theme.danger = style.getPropertyValue('--danger');
			theme.light = style.getPropertyValue('--light');
			theme.dark = style.getPropertyValue('--dark');
			var redIcon = new L.Icon({
				iconUrl: 'css/images/marker-icon-2x-red.png',
				shadowUrl: 'css/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});

			var greenIcon = new L.Icon({
				iconUrl: 'css/images/marker-icon-2x-green.png',
				shadowUrl: 'css/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});

			var orangeIcon = new L.Icon({
				iconUrl: 'css/images/marker-icon-2x-orange.png',
				shadowUrl: 'css/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});

			var announcementsChart = new Chart(
				document.getElementById('announcementsChart').getContext('2d'), {
				type: 'doughnut',
				data: {
					labels: ['High', 'Medium', 'Low'],
					datasets: [{
						backgroundColor: [
							theme.danger,
							theme.warning,
							theme.success,
						],
						borderColor: '#222',
						borderWidth: '4',
						data: <?php echo json_encode($announcementsData); ?>
					}]
				},
				options: {
					legend: {
						display : true,
						labels: {fontColor: theme.light}
					},
					title: {
						display: true,
						text: 'Announcement Levels Distribution',
						fontColor: theme.light,
						fontSize: 16
					},
					animation: {
						animateScale: true,
						animateRotate: true
					},
					plugins: {
						deferred: {
							xOffset: 150,   // defer until 150px of the canvas width are inside the viewport
							yOffset: '50%', // defer until 50% of the canvas height are inside the viewport
							delay: 100      // delay of 500 ms after the canvas is considered inside the viewport
						}
					}
				}
			});

			var helpRequestsStatusChart = new Chart(
				document.getElementById('helpRequestsStatusChart').getContext('2d'), {
				type: 'doughnut',
				data: {
					labels: ['Unsolved', 'In-Progress', 'Solved'],
					datasets: [{
						backgroundColor: [
							theme.danger,
							theme.warning,
							theme.success,
						],
						borderColor: '#222',
						borderWidth: '4',
						data: <?php echo json_encode($helpRequestStatusData); ?>
					}]
				},
				options: {
					legend: {
						display : true,
						labels: {fontColor: theme.light}
					},
					title: {
						display: true,
						text: 'Status Distribution',
						fontColor: theme.light,
						fontSize: 16
					},
					plugins: {
						deferred: {
							xOffset: 150,   // defer until 150px of the canvas width are inside the viewport
							yOffset: '50%', // defer until 50% of the canvas height are inside the viewport
							delay: 100      // delay of 500 ms after the canvas is considered inside the viewport
						}
					}
				}
			});

			var helpRequestsNeedsChart = new Chart(
				document.getElementById('helpRequestsNeedsChart').getContext('2d'), {
				type: 'doughnut',
				data: {
					labels: ['Clothes', 'Food', 'Medicines', 'Others'],
					datasets: [{
						backgroundColor: [
							theme.danger,
							theme.warning,
							theme.success,
							theme.primary,
						],
						borderColor: '#222',
						borderWidth: '4',
						data: <?php echo json_encode($helpRequestNeedsData); ?>
					}]
				},
				options: {
					legend: {
						display : true,
						labels: {fontColor: theme.light}
					},
					title: {
						display: true,
						text: 'Needs Distribution',
						fontColor: theme.light,
						fontSize: 16
					},
					plugins: {
						deferred: {
							xOffset: 150,   // defer until 150px of the canvas width are inside the viewport
							yOffset: '50%', // defer until 50% of the canvas height are inside the viewport
							delay: 100      // delay of 500 ms after the canvas is considered inside the viewport
						}
					}
				}
			});

			var missingPersonsStatusChart = new Chart(
				document.getElementById('missingPersonsStatusChart').getContext('2d'), {
				type: 'doughnut',
				data: {
					labels: ['Missing', 'Found'],
					datasets: [{
						backgroundColor: [
							theme.danger,
							theme.success,
						],
						borderColor: '#222',
						borderWidth: '4',
						data: <?php echo json_encode($missingPersonStatusData); ?>
					}]
				},
				options: {
					legend: {
						display : true,
						labels: {fontColor: theme.light}
					},
					title: {
						display: true,
						text: 'Status Distribution',
						fontColor: theme.light,
						fontSize: 16
					},
					plugins: {
						deferred: {
							xOffset: 150,   // defer until 150px of the canvas width are inside the viewport
							yOffset: '50%', // defer until 50% of the canvas height are inside the viewport
							delay: 100      // delay of 500 ms after the canvas is considered inside the viewport
						}
					}
				}
			});

			var missingPersonsAgeChart = new Chart(
				document.getElementById('missingPersonsAgeChart').getContext('2d'), {
				type: 'doughnut',
				data: {
					// 1-10, 11-20, 21-45, 46+
					labels: ['Child (1-10 yrs)', 'Teenage (11-20 yrs)', 'Adult (21-45 yrs)', 'Elderly (46+ yrs)'],
					datasets: [{
						backgroundColor: [
							theme.danger,
							theme.success,
							theme.primary,
							theme.warning,
						],
						borderColor: '#222',
						borderWidth: '4',
						data: <?php echo json_encode($missingPersonAgeData); ?>
					}]
				},
				options: {
					legend: {
						display : true,
						labels: {fontColor: theme.light}
					},
					title: {
						display: true,
						text: 'Age Distribution',
						fontColor: theme.light,
						fontSize: 16
					},
					plugins: {
						deferred: {
							xOffset: 150,   // defer until 150px of the canvas width are inside the viewport
							yOffset: '50%', // defer until 50% of the canvas height are inside the viewport
							delay: 100      // delay of 500 ms after the canvas is considered inside the viewport
						}
					}
				}
			});
			// Maps Functions
			var india = L.latLng(21.3303150734318, 78.24462890625001);
			var helpRequestMap = L.map('helpRequestMap').setView(india, 5);
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1IjoibmV4dHdhdmVoZWxwZXIiLCJhIjoiY2pybmxwOHFiMHU2bzQ0dGtjN2x1Mjl3ZCJ9.fd5KPogARWQAoX1KtnwZoA'
			}).addTo(helpRequestMap);
			// Help Request Maps Cluster
			var markersHelpRequests = L.markerClusterGroup();
			var pointsHelpRequests = <?php echo json_encode($helpRequestCoordinateData); ?>;
			for (var i = 0; i < pointsHelpRequests.length; i++) {
				var a = pointsHelpRequests[i];
				
				var customIcon;
				if(a[2] == 0) customIcon = greenIcon //solved
				else if(a[2] == 1) customIcon = orangeIcon //in-progress
				else customIcon = redIcon //unsolved
				
				var marker = L.marker(new L.LatLng(a[0], a[1]), {icon: customIcon});
				markersHelpRequests.addLayer(marker);
			}
			helpRequestMap.addLayer(markersHelpRequests);
			// Needed to show map properly - resets when the tab is shown
			$("a[href='#pills-helpRequests']").on('shown.bs.tab', function(e) {
				helpRequestMap.invalidateSize();
			});


			var missingPersonMap = L.map('missingPersonMap').setView(india, 5);
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1IjoibmV4dHdhdmVoZWxwZXIiLCJhIjoiY2pybmxwOHFiMHU2bzQ0dGtjN2x1Mjl3ZCJ9.fd5KPogARWQAoX1KtnwZoA'
			}).addTo(missingPersonMap);
			// Missing Persons Maps Cluster
			var markersMissingPerson = L.markerClusterGroup();
			var addressMissingPersons = <?php echo json_encode($missingPersonMapData); ?>;
			for (var i = 0; i < addressMissingPersons.length; i++) {
				var url = "https://nominatim.openstreetmap.org/search?q="+addressMissingPersons[i]+"&format=json&limit=1"; //TODO - &countrycodes=in
				$.getJSON(url, function (result) {
					if (result.length == 0) {
						return;
					}
					var loc = L.latLng(result[0].lat, result[0].lon);
					var marker = L.marker(loc);
					markersMissingPerson.addLayer(marker);
				});
			}
			missingPersonMap.addLayer(markersMissingPerson);
			// Needed to show map properly - resets when the tab is shown
			$("a[href='#pills-missingPersons']").on('shown.bs.tab', function(e) {
				missingPersonMap.invalidateSize();
			});


			var reliefCampMap = L.map('reliefCampMap').setView(india, 5);
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1IjoibmV4dHdhdmVoZWxwZXIiLCJhIjoiY2pybmxwOHFiMHU2bzQ0dGtjN2x1Mjl3ZCJ9.fd5KPogARWQAoX1KtnwZoA'
			}).addTo(reliefCampMap);
			// Relief Camp Maps Cluster
			var markersReliefCamp = L.markerClusterGroup();
			var pointsReliefCamps = <?php echo json_encode($reliefCampCoordinateData); ?>;
			for (var i = 0; i < pointsReliefCamps.length; i++) {
				var a = pointsReliefCamps[i];
				
				var marker = L.marker(new L.LatLng(a[0], a[1]));
				markersReliefCamp.addLayer(marker);
			}
			reliefCampMap.addLayer(markersReliefCamp);
			// Needed to show map properly - resets when the tab is shown
			$("a[href='#pills-reliefCenterLocations']").on('shown.bs.tab', function(e) {
				reliefCampMap.invalidateSize();
			});
		</script>
	</body>

</html>
<?php
	include('../session.php');
	$query = "SELECT OperationID, OperationName, OperationStartDate, OperationEndDate FROM archivedata;";
	$result = mysqli_query($db, $query);
	if (!$result) {
		echo "Error Fetching!";
		exit();
	}
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$operationName = mysqli_real_escape_string($db, $_POST['name']);
		$endDate = date('Y-m-d');
		$startDate = mysqli_fetch_assoc($result)['OperationStartDate'];

		if($endDate == $startDate){
			echo "<B> Cannot archive for the same EndDate and StartDate! </B>";
			exit();
		}

		$updateQuery = "INSERT INTO `archivedata`
						(`OperationName`, `OperationStartDate`, `OperationEndDate`)
						VALUES ('$operationName', '$startDate', '$endDate');
						UPDATE `archivedata` SET `OperationStartDate`='$endDate'
						WHERE `OperationID` = 1;";

		$updateQueryResult = mysqli_multi_query($db, $updateQuery);
		while (mysqli_next_result($db)) {;} // flush multi_queries

		if (!$updateQueryResult) {
			$error = "Error Updating!";
		} else {
			$error = "Successfully Updated!";
			$result = mysqli_query($db, $query);
			if (!$result) {
				echo "Error Fetching!";
				exit();
			}
		}
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Analytics</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<!-- MDBootstrap Datatables  -->
		<link href="../bootstrap/css/datatables.min.css" rel="stylesheet">
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
			<div class="row">
					<div class="col">
					<h3><a href = "../index.php">⤺ Go Back</a></h3>
					</div>
					<div class="col">
					<h3 ALIGN=RIGHT><a href = "../logout.php">Sign Out <i class="fas fa-sign-out-alt"></i></a></h3>

				</div>
			</div>
			<div class="row justify-content-center" style="margin-bottom: 20px;">
				<form class="form-inline" method="POST" action="index.php">
					<input type="text" name="name" class="col form-control" placeholder="Operation Name" required>
					<button type="submit" class="col btn btn-primary" name="action" value="Update">Archive Current Data</button>
				</form>
			</div>
			<table id="dtable" class="mytable table-dark table-striped table text-center" width="100%">
				<thead>
					<tr>
						<td><strong>Operation Name</strong></td>
						<td><strong>Start Date</strong></td>
						<td><strong>End Date</strong></td>
						<td><strong>Operation</strong></td>
					</tr>
				</thead>
				<tbody>
					<?php
						while ($item = mysqli_fetch_array($result)):
					?>
					<tr>
						<td><?php echo $item['OperationName']; ?></td>
						<td><?php echo $item['OperationStartDate']; ?></td>
						<td><?php echo ($item['OperationEndDate'] == NULL ? '-' : $item['OperationEndDate']); ?></td>
						<td><?php echo "<a href='details.php?id=$item[OperationID]' class='btn btn-light shadow' role='button''><i class='fas fa-search'></i> View Details</a>"; ?></td>
					</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		<!-- MDBootstrap Datatables  -->
		<script type="text/javascript" src="../js/datatables.min.js"></script>
		<script>
			$(document).ready(function () {
				$('#dtable').DataTable();
				$('.dataTables_length').addClass('bs-select');
			});
		</script>
	</body>

</html>

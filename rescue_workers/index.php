<?php
	include('../session.php');

	$rescueWorkers = "SELECT * FROM `rescueworkers`;";
	$rescueWorkersData = mysqli_query($db, $rescueWorkers);
	if (!$rescueWorkersData) {
		echo "Error Fetching!";
		exit();
	}
?>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Rescue Workers</title>

		<!-- Bootstrap core CSS -->
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/index.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

		<!-- Leaflet Maps -->
		<link rel="stylesheet" href="css/leaflet.css">
		<link rel="stylesheet" href="css/MarkerCluster.css">
		<link rel="stylesheet" href="css/MarkerCluster.Default.css">
		<script src="../js/leaflet.js"></script>
        <script src="../js/leaflet.markercluster.js"></script>
        
		<!-- Time related stuff -->
		<script src="../js/moment.min.js"></script>
	</head>

	<body class="text-light">
		<!-- Page Content -->
		<div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <h3><a href = "../index.php">⤺ Go Back</a></h3>
                </div>
                <div class="col text-center"></div>
                <div class="col-lg-3">
                    <h3 ALIGN=RIGHT ><a href = "../logout.php">Sign Out</a></h3>
                </div>
            </div>
			<div class="maps" id="rescueWorkersMap" style="margin-top: 20px"></div>
		</div>

		<!-- Bootstrap core JavaScript -->
		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="../js/remove_banner.js"></script>
		<script>
			$(".alert").delay(2000).slideUp(200, function() {
				$(this).alert('close');
			});
		</script>

		<script>
			var redIcon = new L.Icon({
				iconUrl: 'css/images/marker-icon-2x-red.png',
				shadowUrl: 'css/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
			});

			var greenIcon = new L.Icon({
				iconUrl: 'css/images/marker-icon-2x-green.png',
				shadowUrl: 'css/images/marker-shadow.png',
				iconSize: [25, 41],
				iconAnchor: [12, 41],
				popupAnchor: [1, -34],
				shadowSize: [41, 41]
            });
            
			var india = L.latLng(21.3303150734318, 78.24462890625001);
			var rescueWorkersMap = L.map('rescueWorkersMap').setView(india, 5);
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1IjoibmV4dHdhdmVoZWxwZXIiLCJhIjoiY2pybmxwOHFiMHU2bzQ0dGtjN2x1Mjl3ZCJ9.fd5KPogARWQAoX1KtnwZoA'
			}).addTo(rescueWorkersMap);

			var markersRescueWorker = L.markerClusterGroup();
			var rescueWorkers = <?php echo json_encode(mysqli_fetch_all($rescueWorkersData, MYSQLI_ASSOC)); ?>;
			for (var i = 0; i < rescueWorkers.length; i++) {
                var worker = rescueWorkers[i];
                var custom_icon = redIcon;
                if(worker['HelpRequestID'] == null){
                    custom_icon = greenIcon;
                }
				var marker = L.marker(new L.LatLng(worker['Latitude'], worker['Longitude']), {icon: custom_icon});
				
				var popupContent = "<b> Name: </b>"+worker['WorkerName']
									+" <br> <b>Phone: </b>"+worker['WorkerPhone'];				
                marker.bindPopup(popupContent);
                var timeStamp = new Date(worker['UpdatedAt']);
				// Converting to IST
                timeStamp.setHours( timeStamp.getHours() + 5);
                timeStamp.setMinutes( timeStamp.getMinutes() + 30);
				marker.bindTooltip("<b>Updated: </b>" + moment(timeStamp).fromNow(), {direction:'bottom'});

				markersRescueWorker.addLayer(marker);
			}
			rescueWorkersMap.addLayer(markersRescueWorker);

			function assignWorker(workerName, workerPhone){
				var worker_name_input = document.getElementById("workerName");
				var worker_phone_input = document.getElementById("workerPhone");
				var inprogress_radio_btn = document.getElementById("inprogress");
				worker_name_input.value = workerName;
				worker_phone_input.value = workerPhone;
				inprogress_radio_btn.checked = true;
				$('body').animate({scrollTop:0}, 500, 'swing');
			}
		</script>
	</body>

</html>
